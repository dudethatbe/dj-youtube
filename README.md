## installation

Requires that `youtube-dl` is installed and available to the path
```
$ npm install
```
## usage

### Download a playlist to a folder
```
$ mkdir test
$ npm start -- -o ./test -p ytu.be/...
```
This requires that you create a folder `./test` to save the playlist `ytu.be` contents to.

### Download multiple playlists
```
$ mkdir playlist
$ echo yt.ube/... > playlist/url
$ npm start -- -i ./playlist
```
Unlike the earlier script, this requires that you create a folder for each playlist. Save a `url` file to each folder that contains the playlist URL `ytu.be`. The folder structure only supports one-level deep of folders, so they must all be in the same folder (no subfolders):
```
|
|____house
|      |___url ---> "ytu.be/pl1..."
|
|____techno
|      |___url ---> "ytu.be/pl2..."
|
|____acid
|      |___url ---> "ytu.be/pl3..."
|
|....
|
```

## configuration

The `.env` file contains shortcuts for the `-o`, `-p`, and `-i` arguments when using the `start` command
```
# Single playlist download
OUTPUT=...
PLAYLIST=...

# Multiple playlist download
INPUT=...
```
