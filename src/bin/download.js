import { findFolders, getURL } from "../lib/fileFinder.js";
import { downloadPlaylistTo } from "../lib/playlistDownloader.js";
import { resolve } from "path";
async function download(url, saveTo) {
  console.log(`downloading ${url} to ${saveTo} ...`);
  try {
    await downloadPlaylistTo(saveTo, url);
  } catch (e) {
    console.error(e.stderr);
    if (e.stderr.startsWith("WARNING: unable to download video info webpage")) {
      console.warn("Unable to access video");
      console.warn(e.stdout);
    } else if (e.stderr.startsWith("ERROR: Video unavailable")) {
      console.warn("Skipping unavailable video");
      console.warn(e.stdout);
    } else {
      throw e;
    }
  }
}
export async function downloadPlaylist(url, saveTo, tryAgain = false) {
  if (!tryAgain) {
    return download(url, saveTo);
  } else {
    let tries = 3;
    let downloaded = false;
    while (tries > 0 && !downloaded) {
      try {
        await download(url, saveTo);
        downloaded = true;
      } catch (e) {
        if (
          e.stderr.startsWith(
            "ERROR: unable to download video data: HTTP Error 403: Forbidden"
          )
        ) {
          // try again ?
          console.warn("Access Denied! Setting timer and trying again ...");
          await backoff(4000);
        } else {
          throw e;
        }
      }
      tries--;
    }
  }
}
async function backoff(timerMs) {
  return new Promise(function (resolve) {
    setTimeout(
      (r) => {
        r();
      },
      timerMs,
      resolve
    );
  });
}
export async function downloadPlaylists(readFrom) {
  const folders = await findFolders(readFrom);
  for (const folder of folders) {
    const fullPath = resolve(readFrom, folder.name);
    const urlFile = resolve(fullPath, "url");
    var url;
    try {
      url = await getURL(urlFile);
    } catch (e) {
      if (e.code !== "ENOENT") {
        throw e;
      } else {
        console.warn(`SKIPPING; Unable to find "url" file in ${folder.name}`);
        continue;
      }
    }
    await download(url, fullPath);
  }
}
