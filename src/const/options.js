export default {
  groups: {
    pathOptions: { group: "Path Options" },
    downloadOptions: { group: "Download Options" },
    emptyOptions: { group: "" },
  },
  args: {
    input: {
      names: ["input", "i"],
      type: "string",
      env: "INPUT",
      help: "Input folder hosting playlist-url's",
      helpArg: "PATH",
    },
    output: {
      names: ["output", "o"],
      type: "string",
      env: "OUTPUT",
      help: "Output folder to store archive(s)",
      helpArg: "PATH",
      default: ".",
    },
    playlist: {
      names: ["playlist", "p"],
      type: "string",
      env: "PLAYLIST",
      help: "Playlist URL to downlaod from",
      helpArg: "URL",
    },
    help: {
      name: "help",
      type: "bool",
      help: "show the help text",
    },
  },
};
