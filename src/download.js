import dotenv from "dotenv";
dotenv.config();
import { createParser } from "dashdash";
import json from "./const/options.js";
import { downloadPlaylist, downloadPlaylists } from "./bin/download.js";
const { groups, args } = json;
const options = [
  groups.downloadOptions,
  args.playlist,
  groups.pathOptions,
  args.input,
  args.output,
  groups.emptyOptions,
  args.help,
];
(async () => {
  const parser = createParser({ options: options });
  const opts = parser.parse(process.argv);
  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(`usage: npm start -- [OPTIONS]\noptions:\n${help}`);
    return;
  }
  if (opts.playlist && opts.output) {
    // download single playlist
    await downloadPlaylist(opts.playlist, opts.output);
  } else if (opts.input) {
    // download multiple playlist
    await downloadPlaylists(opts.input);
  }
})();
