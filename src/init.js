import dotenv from "dotenv";
dotenv.config();
import { createParser } from "dashdash";
import json from "./const/options.js";
import { makePlaylistFolders } from "./lib/fileFinder.js";
import { downloadPlaylists } from "./bin/download.js";
const { groups, args } = json;
const options = [
  groups.pathOptions,
  args.input,
  args.output,
  groups.emptyOptions,
  args.help,
];
import Papa from "papaparse";
import { readFile } from "fs/promises";
const { parse } = Papa;

(async () => {
  const parser = createParser({ options: options });
  const opts = parser.parse(process.argv);

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(`usage: npm run init -- [OPTIONS]\noptions:\n${help}`);
    return;
  }

  if (opts.input && opts.output) {
    const saveTo = opts.output;
    // const saveTo = "/mnt/d/oldytplaylists/";

    const csvFile = opts.input;
    // const csvFile = "./src/const/playlists.csv";

    const fileData = await readFile(csvFile, "utf8");
    const data = parse(fileData, { header: true, delimiter: "," });
    // console.log(data.data);

    console.log(`making ${data.data.length} playlists folders`);
    await makePlaylistFolders(saveTo, data.data);

    console.log(`now downloading playlists saved in "${saveTo}"`);
    await downloadPlaylists(saveTo);
    console.log("finished");
  }
})();
