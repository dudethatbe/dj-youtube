import { mkdir, readdir, open, readFile, writeFile } from "fs/promises";
import { join } from "path";
export async function findFolders(pathToFolders) {
  const dirents = await readdir(pathToFolders, { withFileTypes: true });
  const folders = dirents.filter((f) => f.isDirectory());
  return folders;
}
export async function getURL(urlFile) {
  const file = await open(urlFile);
  const url = await file.readFile({ encoding: "utf8" });
  await file.close();
  return url;
}
export async function makePlaylistFolders(pathToFolders, playlists) {
  for (const playlist of playlists) {
    let playlistFolder = join(pathToFolders, playlist.name);
    try {
      await mkdir(playlistFolder);
      console.log(`created folder ${playlistFolder}`);
    } catch (e) {
      if (e.code !== "EEXIST") {
        console.error(`Unexpected error while creating folder`);
        throw e;
      }
    }
    let urlFile = join(playlistFolder, "url");
    try {
      await writeFile(urlFile, playlist.url);
      console.log(`wrote "${playlist.url}" to ${urlFile}`);
    } catch (e) {
      console.error(`Unexpected error while writing url to "${urlFile}"`);
      throw e;
    }
  }
}
