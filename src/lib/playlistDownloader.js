import { promisify } from "util";
import { exec } from "child_process";
const execCmd = promisify(exec);
export async function downloadPlaylistTo(folder, url) {
  const archive = `${folder}/archive`;
  const command = `youtube-dl --restrict-filenames -i -x --audio-format "mp3" --embed-thumbnail --yes-playlist --rm-cache-dir --download-archive ${archive} ${url}`;
  const { stderr, stdout } = await execCmd(command, { cwd: folder });
  if (stderr) {
    console.error(stderr);
  }
  if (stdout) {
    console.log(stdout);
  }
}
